# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Assignment 2 Requirements:

Three Parts

1. Version Control with Git and Bitbucket
2. Git commands and commits

#### README.md file should include the following items:

* Breakdown of assignment
* img folder containing - "Querybook" screenshot
* README.md file, with images

#### The purpose of this assignment is to:

1. Practice git commands and file management
2. Practice pushing files to Bitbucket using git

#### Assignment Breakdown:
1. http://localhost:9999/hello/
	a. HelloHome.html
	b. sayhello
	c. querybook.html
	d. sayih

#### Assignment Screenshots:

*Screenshot of Querybook*:

![Querybook Screenshot](img/Screenshot_1.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")