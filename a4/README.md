# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Assignment 4 Requirements:

* Practice client and server side validation

#### Assignment 4 should include the following items:

* index.html and thanks.html
* updated web.xml and java servlet files
* img folder containing - a4 client side and a4 server side

#### The purpose of this assignment is to:

1. Practice matching server-side validation with client-side validation

#### Assignment Screenshots:

*Screenshot of a4 client side and a4 server side*:

![client Screenshot](img/Screenshot_1.jpg)
![server Screenshot](img/Screenshot_2.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")