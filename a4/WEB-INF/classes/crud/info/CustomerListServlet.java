class Car extends Vehicle
{
    private float speed;


    //default constructor
    public Car()
    {
        super();
        System.out.println("\nInside car default constructor.");
        //super(); //will generate error!
        speed = 100;
    }

    //parameterized constructor
    public Car(String m, String d, int y, float s)
    {
        super(m, d, y);
        System.out.println("\nInside car constructor with parameters.");
        speed = s;        
    }




    //getter/setter methods (accessor/mutator methods)
    public double getSpeed()
    {
        return speed;
    }

    public void setSpeed(float s)
    {
        speed = s;
    }

    //subclasses can override (replace) inherited method--that is, subclasses version of method is called istead.
    //overridden method must have *same signature
}