// save as "<TOMCAT_HOME>\webapps\a4\WEB-INF\classes\crud\info\CustomerListServlet.java"
package crud.info;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
 
import crud.business.Customer;

@WebServlet("/customerList")
public class CustomerListServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
			String url = "/index.html"; //initialize url value (used for logic below)
        
        // get current action
        String action = request.getParameter("action");
        if (action == null) {
            action = "join";  // default action
        }

        // perform action and set URL to appropriate page
        if (action.equals("join")) {
            url = "/index.jsp";    // the "join" page
        } 
        else if (action.equals("add")) {
            // get parameters from the request
            String firstName = request.getParameter("fname");
            String lastName = request.getParameter("lname");
            String street = request.getParameter("street");
            String city = request.getParameter("city");
            String state = request.getParameter("state");
            String zip = request.getParameter("zip");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            String balance = request.getParameter("balance");
            String totalSales = request.getParameter("total_Sales");
            String notes = request.getParameter("notes");

            // store data in Customer object
            Customer user = new Customer(firstName, lastName, street, city, state, zip, phone, email, balance, totalSales, notes);

            // validate the parameters
				//empty string: string with zero length.
				//null value: is unknown value--not having a string.
            String message;
            if (firstName == null || lastName == null || street == null || city == null || state == null || zip == null ||  email == null || balance == null || totalSales == null ||
                firstName.isEmpty() || lastName.isEmpty() || street.isEmpty() || city.isEmpty() || state.isEmpty() || zip.isEmpty() || email.isEmpty() || balance.isEmpty() || totalSales.isEmpty()) {
                message = "All text boxes required except Notes.";
                url = "/index.jsp";
            } 
            else {
                message = "";
                url = "/thanks.jsp";
            }
            request.setAttribute("user", user);
            request.setAttribute("message", message);
        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }    
}
