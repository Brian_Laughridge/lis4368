# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Project 2 Requirements:

1. Complete the JSP/Servlets web application using the MVC framework.
2. Provide Create, Read, Update, and Delete (CRUD) functionality.
3. In addition a search (SCRUID) option could be added, and which will be left as a research exercise.

#### README.md file should include the following items:

* p2 folder containing - global and WEB-INF folders and customer, index, modify, and thanks, .jsp files.
* README.md file, with images of validation

#### The purpose of this project is to:
1. Practice adding a database table and CRUD functionality
2. Create a web application to create, modify, read, and delete database entries.


#### Assignment Screenshots:

*Valid User Form Entry*:

![Valid User Form Entry](img/screenshot_1.jpg)
![Passed Validation Screenshot](img/screenshot_2.jpg)

*Display Data*:
![Display Data](img/screenshot_3.jpg)

*Modify Form*:
![Modify Form](img/screenshot_4.jpg)
![Modified Data](img/screenshot_5.jpg)
![Delete Warning](img/screenshot_6.jpg)

*Associated Database Changes*:
![Database Changes](img/screenshot_7.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
