# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Project 1 Requirements:

Three Parts

1. Version Control with Git and Bitbucket
2. Create Web Data Interface with Data Validation
#### README.md file should include the following items:

* p1 folder containing - global and p1_student_files
* global folder containing - nav.jsp, nav_global.jsp
* p1_student_files containing - css, fonts, global header and footer file, js, and index.jsp
* img folder containing - Failed and Passed Validation images
* README.md file, with images of validation

#### The purpose of this project is to:
1. Practice client side validation using bootstrap and html
2. Practice pushing files to Bitbucket using git

#### Git commands w/short descriptions:

1. git init - create a new local repo
2. git status - list the files you've changed and those you still need to add or commit
3. git add - Adds files
4. git commit - commit changes to repo
5. git push - send changes to the master branch
6. git pull - update from the remote repo
7. git checkout -b - create a new branch and switch to it

#### Assignment Screenshots:

*Screenshot of Validation*:

![Failed Validation Screenshot](img/screenshot_1.jpg)
![Passed Validation Screenshot](img/screenshot_2.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
