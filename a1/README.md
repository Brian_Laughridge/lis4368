# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Assignment 1 Requirements:

Three Parts

1. Version Control with Git and Bitbucket
2. Git commands and commits

#### README.md file should include the following items:

* bitbucketstationlocations repo
* myteamquotes repo
* img folder containing - "Hello World" screenshot and tomcat screenshot
* README.md file, with images

#### The purpose of this assignment is to:

1. Practice git commands and file management
2. Practice pushing files to Bitbucket using git

#### Git commands w/short descriptions:

1. git init - create a new local repo
2. git status - list the files you've changed and those you still need to add or commit
3. git add - Adds files
4. git commit - commit changes to repo
5. git push - send changes to the master branch
6. git pull - update from the remote repo
7. git checkout -b - create a new branch and switch to it

#### Assignment Screenshots:

*Screenshot of Hello World & Tomcat*:

![Hello World Screenshot](img/Screenshot_1.jpg)
![Tomcat Screenshot](img/Screenshot_2.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")