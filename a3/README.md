# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Assignment 3 Requirements:

* Create and forward engineer petstore database

#### README.md file should include the following items:

* a3.mwb file link
* a3.sql file link
* img folder containing - "a3.png" entity relationship diagram

#### The purpose of this assignment is to:

1. Practice using mySQL Workbench and SQL commands

#### Assignment Screenshots:

*Screenshot of a3 entity relationship diagram*:

![ERD Screenshot](img/Screenshot_1.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")