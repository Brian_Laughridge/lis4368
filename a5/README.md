# LIS4368 - Advanced Web Applications Development

## Brian Laughridge

### Assignment 5 Requirements:

* Practice client and server side validation

#### Assignment 5 should include the following items:

* index.html and thanks.html
* updated web.xml and java servlet files
* img folder containing - a5 client side, a5 server side and database update

#### The purpose of this assignment is to:

1. Practice matching server-side validation with client-side validation
2. Update a local database using html and java servlets

#### Assignment Screenshots:

*Screenshot of a5 client side, a5 server side, and database update*:

![Client Screenshot](img/Screenshot_1.jpg)
![Server Screenshot](img/Screenshot_2.jpg)
![Database Update](img/Screenshot_3.jpg)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")